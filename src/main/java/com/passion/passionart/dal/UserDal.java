package com.passion.passionart.dal;

import com.passion.passionart.model.User;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public interface UserDal {

    List<User> getAllUser() throws Exception;

    User update(Query findByQuery, Update updateData) throws Exception;

    User findUserByUsername(String username) throws Exception;
    void save(User user) throws Exception;
    void saveAll(List<User> user) throws Exception;


}
