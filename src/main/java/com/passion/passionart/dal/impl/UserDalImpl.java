package com.passion.passionart.dal.impl;


import com.passion.passionart.dal.UserDal;
import com.passion.passionart.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDalImpl implements UserDal {


    private final MongoTemplate mongoTemplate;

    @Autowired
    public UserDalImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public void save(User user) throws Exception {
        mongoTemplate.save(user);
    }

    public void saveAll(List<User> users) throws Exception {
        for(User user: users)
            save(user);
    }

    @Override
    public List<User> getAllUser() throws Exception {
        return mongoTemplate.findAll(User.class);
    }

    @Override
    public User update(Query findByQuery, Update updateData) throws Exception {
        return mongoTemplate.findAndModify(findByQuery, updateData,new FindAndModifyOptions().returnNew(true), User.class);
    }

    @Override
    public User findUserByUsername(String username) throws Exception {
        return mongoTemplate.findById(username,User.class);
    }


}
