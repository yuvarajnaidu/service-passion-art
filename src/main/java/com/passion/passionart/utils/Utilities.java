package com.passion.passionart.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

@Component
public class Utilities {

    private Pattern pattern = Pattern.compile("y|n|Y|N");
    private String regexStart = Pattern.quote("{{");
    private String regexEnd = Pattern.quote("}}");
    private Pattern imagePattern = Pattern.compile(regexStart + "(.*?)" + regexEnd);

    private Logger logger = LoggerFactory.getLogger(Utilities.class);

    public Date parseDateFromString(String pattern, String date) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.parse(date);
    }
}