package com.passion.passionart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PassionArtApplication {

	public static void main(String[] args) {
		SpringApplication.run(PassionArtApplication.class, args);
	}

}
