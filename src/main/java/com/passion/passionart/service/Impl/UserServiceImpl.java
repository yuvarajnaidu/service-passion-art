package com.passion.passionart.service.Impl;

import com.passion.passionart.dal.UserDal;
import com.passion.passionart.enums.RolesEnum;
import com.passion.passionart.model.ApiResponse;
import com.passion.passionart.model.User;
import com.passion.passionart.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDal userDal;

    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);


    @Override
    public User updateUserLastLogin(String username, Date date) throws Exception {
        Query findById = new Query();
        findById.addCriteria(Criteria.where("_id").is(username));
        Update updateUserLastLogin = new Update();
        updateUserLastLogin.set("last_login", date);
        return userDal.update(findById, updateUserLastLogin);
    }

    @Override
    public ApiResponse saveUser(User user) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        if (null != userDal.findUserByUsername(user.getUsername())) {
            apiResponse.setSuccessful(false);
            apiResponse.setCode(409);
            apiResponse.setMessage("Username Exist");
            return apiResponse;
        }
        user.setAuthorities(new ArrayList<>());
        user.getAuthorities().add(RolesEnum.USER.getRole());
        user.setRecordCreateDate(new Date(System.currentTimeMillis()));
        user.setLastLogin(new Date(System.currentTimeMillis()));
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        try{
            userDal.save(user);
            apiResponse.setSuccessful(true);
            apiResponse.setMessage("User Created");
            return apiResponse;
        }catch (Exception e){
            apiResponse.setSuccessful(false);
            apiResponse.setMessage("Please Contact Support");
            logger.error(e.getMessage());
            return apiResponse;
        }
    }

    @Override
    public User findUserByUsername(String username) throws Exception {
        return userDal.findUserByUsername(username);
    }

    @Override
    public ApiResponse checkUserName(String username) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        if(null!= findUserByUsername(username)){
            apiResponse.setSuccessful(false);
            apiResponse.setCode(409);
            apiResponse.setMessage("Username Exist");
        }else{
            apiResponse.setSuccessful(true);
            apiResponse.setCode(200);
        }
        return apiResponse;
    }
}
