package com.passion.passionart.service.Impl;

import com.passion.passionart.enums.RolesEnum;
import com.passion.passionart.model.User;
import com.passion.passionart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SecurityService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            User user = userService.findUserByUsername(username);

            if (null == user) {
                throw new UsernameNotFoundException("User not found");
            } else {
                if (null == user.getAuthorities() || user.getAuthorities().isEmpty() || !user.getAuthorities().contains(RolesEnum.USER.getRole()))
                    throw new UsernameNotFoundException("User does not have roles");
            }

            List<SimpleGrantedAuthority> authorities = new ArrayList<>();

            for (String role : user.getAuthorities()) {
                authorities.add(new SimpleGrantedAuthority(role));
            }

            return new org.springframework.security.core.userdetails.User(username, user.getPassword(), authorities);
        } catch (Exception e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("User not found");
        }
    }
}
