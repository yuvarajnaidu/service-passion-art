package com.passion.passionart.service;

import com.passion.passionart.model.ApiResponse;
import com.passion.passionart.model.User;
import org.springframework.http.HttpStatus;

import java.util.Date;

public interface UserService {

    User updateUserLastLogin(String username, Date date) throws Exception;
    ApiResponse saveUser(User user) throws Exception;
    User findUserByUsername(String username) throws Exception;
    ApiResponse checkUserName(String username) throws Exception;
}
