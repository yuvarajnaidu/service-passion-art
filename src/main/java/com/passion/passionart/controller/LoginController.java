package com.passion.passionart.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.passion.passionart.configuration.JwtConfiguration;
import com.passion.passionart.model.ApiResponse;
import com.passion.passionart.model.User;
import com.passion.passionart.model.UserCredentials;
import com.passion.passionart.service.UserService;
import com.passion.passionart.utils.Constants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/api/users") /*rootpath*/
@Controller
public class LoginController {

    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtConfiguration jwtConfiguration;

    @Autowired
    private UserService userService;

    @Value("${nedas.runconfig.lanmode:false}")
    private boolean lanMode;

    @RequestMapping(value = "authenticate", method = RequestMethod.POST)
    public void message(HttpServletRequest request, HttpServletResponse response) {
        try {

            UserCredentials userCredentials = new ObjectMapper().readValue(request.getInputStream(), UserCredentials.class);

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userCredentials.getUsername(), userCredentials.getPassword(), Collections.emptyList());

            try {
                authenticationManager.authenticate(authToken);
            } catch (AuthenticationException e) {
                logger.debug(String.format("user %s authentication failed!", userCredentials.getUsername()));
                unsuccessful(response);
                return;
            }

            if (authToken.isAuthenticated()) {
                success(userCredentials, response);
            } else {
                unsuccessful(response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
    }

    private void success(UserCredentials userCredentials, HttpServletResponse response) throws Exception {
        logger.debug(String.format("user %s authenticated successfully!", userCredentials.getUsername()));

        User user =  userService.updateUserLastLogin(userCredentials.getUsername(), DateTime.now().toDate());
        String role = null;

        String token = generateJwtToken(userCredentials,user, Collections.EMPTY_LIST);
        if (null != token) {
//            final Cookie cookie = new Cookie("passion-x-token", jwtConfiguration.getPrefix().trim() + token);
//            cookie.setDomain("");
//            if(lanMode){
//                cookie.setSecure(true);
//                cookie.setHttpOnly(true);
//            }
//            cookie.setMaxAge(60 * 60);
//            cookie.setPath("/");
//            response.addCookie(cookie);
             response.addHeader(jwtConfiguration.getHeader(), jwtConfiguration.getPrefix() + token);

            response.setStatus(HttpStatus.OK.value());
            response.setContentType("application/json");
            final ObjectNode jsonResponse = JsonNodeFactory.instance.objectNode();
            jsonResponse.put("token", jwtConfiguration.getPrefix() + token);
            ApiResponse apiResponse = new ApiResponse();
            apiResponse.setSuccessful(true);
            apiResponse.setObject(jsonResponse);
            response.getWriter().print(new ObjectMapper().writeValueAsString(apiResponse));
            response.getWriter().flush();
        } else {
            unsuccessful(response);
        }
    }

    private void unsuccessful(HttpServletResponse response) throws Exception {
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json");
        final ObjectNode jsonResponse = JsonNodeFactory.instance.objectNode();
        jsonResponse.put("authenticated", Boolean.FALSE.booleanValue());
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setSuccessful(false);
        apiResponse.setObject(jsonResponse);
        response.getWriter().print(new ObjectMapper().writeValueAsString(apiResponse));
        response.getWriter().flush();
    }

    private String generateJwtToken(UserCredentials userCredentials, User user, List<GrantedAuthority> list) throws Exception {
        String token;

        long now = System.currentTimeMillis();
        token = Jwts.builder()
                .setSubject(userCredentials.getUsername())
                .claim(Constants.CLAIM_AUTHORITIES, list.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .claim(Constants.CLAIM_LAST_LOGIN, user.getLastLogin().getTime())
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + jwtConfiguration.getExpiration() * 1000))  // in milliseconds
                .signWith(SignatureAlgorithm.HS512, jwtConfiguration.getSecret().getBytes())
                .compact();

        return token;
    }


}
