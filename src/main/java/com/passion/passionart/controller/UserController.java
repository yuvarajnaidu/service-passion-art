package com.passion.passionart.controller;

import com.passion.passionart.model.ApiResponse;
import com.passion.passionart.model.User;
import com.passion.passionart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import javax.validation.Valid;

@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "saveUser", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ApiResponse saveUser(@Valid @RequestBody User user) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            return userService.saveUser(user);
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setSuccessful(false);
            return apiResponse;
        }
    }

    @RequestMapping(value = "usernameCheck", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    ApiResponse usernameCheck(@RequestBody  User user) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        try {
            return userService.checkUserName(user.getUsername());
        } catch (Exception e) {
            e.printStackTrace();
            apiResponse.setSuccessful(false);
            return apiResponse;
        }
    }

}
