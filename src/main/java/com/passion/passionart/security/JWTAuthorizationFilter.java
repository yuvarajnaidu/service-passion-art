package com.passion.passionart.security;

import com.passion.passionart.configuration.JwtConfiguration;
import com.passion.passionart.model.User;
import com.passion.passionart.service.Impl.SecurityService;
import com.passion.passionart.service.UserService;
import com.passion.passionart.utils.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authManager, JwtConfiguration jwtConfiguration, JwtTokenProvider tokenProvider, SecurityService securityService, UserService userService) {
        super(authManager);
        this.jwtConfiguration = jwtConfiguration;
        this.tokenProvider = tokenProvider;
        this.securityService = securityService;
        this.userService = userService;
    }

    private JwtTokenProvider tokenProvider;

    private SecurityService securityService;

    private UserService userService;

    private JwtConfiguration jwtConfiguration;

    private static final Logger logger = LoggerFactory.getLogger(JWTAuthorizationFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
/*
        boolean thisUrlWasAuthorized = false;
*/
        try {
            String jwt = null;
            String token = null;
            token = request.getHeader("token");

            if (!StringUtils.isEmpty(token))
                jwt = getJwtFromCookie(token);

            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                String userId = tokenProvider.getUserIdFromJWT(jwt);
                UserDetails userDetails = securityService.loadUserByUsername(userId);

                User passionUser = userService.findUserByUsername(userId);

                Long lastLogin = (Long) tokenProvider.getClaimFromToken(Constants.CLAIM_LAST_LOGIN, jwt);

                if (StringUtils.isEmpty(lastLogin) || null == passionUser.getLastLogin())
                    throw new Exception("invalid last login claim , or user value");

                if (lastLogin != passionUser.getLastLogin().getTime())
                    throw new Exception("Invalid user login");

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
/*
                thisUrlWasAuthorized = true;
*/
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Could not set user authentication in security context", ex);
        }

        filterChain.doFilter(request, response);

       /* if (thisUrlWasAuthorized)
            logger.info("i have the response for "+request.getRequestURI());*/
    }

    private String getJwtFromCookie(String token) throws Exception {
        String jwt = null;
        if (StringUtils.hasText(token) && token.startsWith(jwtConfiguration.getPrefix())) {
            jwt = token.substring(jwtConfiguration.getPrefix().length());
        }
        return jwt;
    }
}